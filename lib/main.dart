import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

double angle = 0;
double scale = 1;
double opacity = 1;

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Widget of the week 4'),
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            ListTile(
              leading: Icon(Icons.add),
              title: Text('inside drawer'),
            ),
            ListTile(
              leading: Icon(Icons.add),
            ),
            ListTile(
              leading: Icon(Icons.add),
            ),
          ],
        ),
      ),
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              AnimatedOpacity(
                duration: Duration(seconds: 3),
                opacity: opacity,
                child: ClipOval(
                  child: Container(
                    height: 200,
                    width: 300,
                    color: Colors.green,
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          opacity = 0;
                        });
                      },
                      child: Center(child: Text('Tap to vanish me')),
                    ),
                  ),
                ),
              ),
              Transform.scale(
                scale: scale,
                child: Transform.rotate(
                  angle: angle,
                  child: Container(
                    height: 200,
                    width: 200,
                    color: Colors.red,
                    child: Text('Some text'),
                  ),
                ),
              ),
              RaisedButton(
                onPressed: () {
                  setState(() {
                    angle += 0.1;
                  });
                },
                child: Text('Rotate red block'),
              ),
              RaisedButton(
                onPressed: () {
                  setState(() {
                    scale += 0.1;
                  });
                },
                child: Text('Make bigger red block'),
              ),
              RaisedButton(
                onPressed: () {
                  setState(() {
                    scale -= 0.1;
                  });
                },
                child: Text('Make smoller red block'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
